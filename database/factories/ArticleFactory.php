<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Article::class, function (Faker\Generator $faker) {


    return [
        'user_id' => $faker->randomElement(App\User::pluck('id')->toArray()),
        'title'=>$faker->text(100),
        'sub_title'=>$faker->text(50),
        'summery'=>$faker->sentence(10),
        'slug'=>$faker->slug,
        'details'=>$faker->paragraph,
        'display'=>$faker->randomElement(['Y','N']),
        'created_at'=>$faker->dateTimeThisYear,

    ];
});
