<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Profile::class, function (Faker\Generator $faker) {


    return [
        'first_name' => $faker->title,
        'last_name'=>$faker->lastName,
        'gender'=>$faker->randomElement(['male','female']),
        'father_name'=>$faker->titleMale.' '.$faker->firstNameMale,
        'mother_name'=>$faker->titleFemale.' '.$faker->firstNameFemale,
        'mobile'=>$faker->unique()->phoneNumber,
        'address'=>$faker->address,
        'website'=>$faker->url,
        'facebook'=>'https://www.facebook.com/'.$faker->unique()->firstName,
        'git'=>'https://www.git'.$faker->randomElement(['lab','hub']).'.com/'.$faker->unique()->firstName,
        'linkedin'=>'https://www.linkedin.com/'.$faker->unique()->firstName,
    ];
});
