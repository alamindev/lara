<?php

use Illuminate\Database\Seeder;
use App\Article;
use App\User;

class ArticleTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::all()->each(function ($user)
        {
            $user->articles()->saveMany(factory(Article::class,4)->make());
        });
    }
}
