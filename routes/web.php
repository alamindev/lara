<?php


Auth::routes();
Route::get('/', 'HomeController@home');
Route::get('/catid/{id}/article/{slug}', 'HomeController@show');
Route::get('/profession/article/{id}', 'HomeController@articlebyProfession');
Route::get('/user/article/{id}', 'HomeController@articlebyUser');
Route::get('/home', 'HomeController@index');
Route::get('/dashboard', 'HomeController@index');
Route::get('/user/{id}/delete', 'UserController@destroy');
Route::resource('user','UserController');
Route::resource('profession','ProfessionController');
Route::resource('video','VideoController');
Route::resource('gallery','GalleryController');
Route::resource('article','ArticleController');

