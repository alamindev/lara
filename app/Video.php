<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable=['user_id','title','summery','source','provider','display'];
    public function galleries()
    {
        return $this->belongsToMany(Gallery::class);
    }
}
