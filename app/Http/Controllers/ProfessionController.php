<?php

namespace App\Http\Controllers;

use App\Profession;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ProfessionController extends Controller
{

    public function index()
    {
        $profession=Profession::all();
        return view('admin.module.profession.index',['professions'=>$profession]);
    }


    public function create()
    {
        return view('admin.module.profession.create');
    }

    public function store(Request $request)
    {
        $profession=$request->only('name');
        Profession::create($profession);
        Session::flash('message','Dta Added '.$request->name. ' Successfully');
        return redirect('/profession');
    }

    public function show(Profession $profession)
    {
      $videos= $profession->userVideos()->get();
        return view('admin.module.profession.show',['profession'=>$profession],compact('videos'));
    }

    public function edit($id)
    {
        $profession=Profession::findOrfail($id);
        return view('admin.module.profession.edit',['profession'=>$profession]);
    }

    public function update(Request $request, $id)
    {
        $userProfession = Profession::findOrfail($id);
        $profession=$request->only('name');
        $userProfession->update($profession);
        Session::flash('message', 'Data Update Successfully');
        return redirect('/profession');
    }

    public function destroy($id)
    {
        Profession::destroy($id);
        Session::flash('message', 'Data Delete Successfully');
        return redirect('/profession');
    }

}
