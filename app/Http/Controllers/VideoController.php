<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Http\Requests\VideoRequest;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class VideoController extends Controller
{
    public function index()
    {
        $video=Video::with('galleries')->get();
        return view('admin.module.video.index',['videos'=>$video]);
    }

    public function create()
    {
        $galleries=Gallery::pluck('name','id');
        return view('admin.module.video.create',compact('galleries'));
    }

    public function store(VideoRequest $request)
    {
        $videoInput=$request->only('title','summery','source','provider','display');
        $videoInput['user_id'] = Auth::id();
        $video=Video::create($videoInput);
        $video->galleries()->attach($request->gallery_id);
        Session::flash('message','Dta Added '.$request->title. ' Successfully');
        return redirect('/video');
    }

    public function show(Video $video)
    {
        return view('admin.module.video.show',compact('video'));
    }

    public function edit(Video $video)
    {
        $galleries=Gallery::pluck('name','id');
        $selected=$video->galleries()->pluck('id')->toArray();
        return view('admin.module.video.edit',compact('video','galleries','selected'));
    }

    public function update(VideoRequest $request, Video $video)
    {
        $videoInput=$request->only('title','summery','source','provider','display');
        $videoInput['user_id'] = Auth::id();
        $video->update($videoInput);
        $gallery=$request->input('gallery_id');
        $video->galleries()->sync($gallery);
        Session::flash('message', 'Data Update Successfully');
        return redirect('/video');
    }

    public function destroy($id)
    {
        video::destroy($id);
        Session::flash('message', 'Data Delete Successfully');
        return redirect('/video');
    }
}
