<?php

namespace App\Http\Controllers;

use App\Article;
use App\Comment;
use App\Profession;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }
    public function home()
    {
        $articles=Article::with('user')->paginate(5);
        $professions=Profession::all();
        return view('blog.article',compact('articles','professions'));
    }
    public function show($id='',$slug='',Comment $comment)
    {
        $article=Article::findOrFail($id);
        $comments=$comment->all()->where('article_id',$id);
        return view('blog.show',compact('article','comments'));
    }
    public function articlebyProfession($id)
    {
        $professions=Profession::all();
        $profession=Profession::findOrfail($id);
        $articles= $profession->articles()->latest('created_at')->paginate(4);
        return view('blog.article',compact('articles','professions'));
    }
    public function articlebyUser($id)
    {
        $professions=Profession::all();
        $user=User::findOrfail($id);
        $articles= $user->articles()->latest('created_at')->paginate(2);
        return view('blog.article',compact('articles','professions'));
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
}
