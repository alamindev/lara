<?php

namespace App\Http\Controllers;

use App\gallery;
use App\Http\Requests\GalleryRequest;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class GalleryController extends Controller
{

    public function index()
    {
        $gallery=gallery::all();
        return view('admin.module.gallery.index',['gallerys'=>$gallery]);
    }

    public function create()
    {
        $videos=Video::all();
        return view('admin.module.gallery.create',compact('videos'));
    }

    public function store(GalleryRequest $request)
    {
        $gallery=Gallery::create($request->all());
        $gallery->videos()->attach($request->video_id);
        Session::flash('message','Dta Added '.$request->title. ' Successfully');
        return redirect('/gallery');
    }

    public function show(Gallery $gallery)
    {
        $videos=$gallery->videos()->get();
        return view('admin.module.gallery.show',['gallery'=>$gallery],compact('videos'));
    }

    public function edit(Gallery $gallery)
    {
        $videos=$videos=Video::all();;
        $selected=$gallery->videos()->pluck('id')->toArray();

        return view('admin.module.gallery.edit',['gallery'=>$gallery],compact('videos','selected'));
    }

    public function update(GalleryRequest $request, Gallery $gallery)
    {
        $gallery->update($request->all());
        Session::flash('message', 'Data Update Successfully');
        return redirect('/gallery');
    }

    public function destroy(Gallery $gallery)
    {
      $gallery->delete();
        Session::flash('message', 'Data Delete Successfully');
        return redirect('/gallery');
    }
}
