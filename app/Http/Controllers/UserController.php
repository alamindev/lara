<?php

namespace App\Http\Controllers;

use App\Profession;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{

    public function index()
    {
        $users = User::with('profile', 'profession')->get();
        return view('admin.module.user.index', ['users' => $users]);
    }


    public function create()
    {
        $profession = Profession::pluck('name', 'id');
        return view('admin.module.user.create')->with('profession', $profession);
    }

    public function store(Request $request)
    {
        $userdata = $request->only('name', 'email', 'profession_id');
        $userdata['password'] = Hash::make($request->password);
        $usersave = User::create($userdata);
        $profiledata = $request->only('first_name', 'last_name', 'gender', 'father_name', 'mother_name', 'mobile', 'address', 'website', 'facebook', 'git', 'linkedin');
        $usersave->profile()->create($profiledata);
        Session::flash('message', 'Dta Added ' . $request->name . ' Successfully');
        return redirect('/user');
    }

    public function show($id)
    {
        $users = User::findOrfail($id);
        return view('admin.module.user.show', ['user' => $users]);
    }

    public function edit($id)
    {
        $profession = Profession::pluck('name', 'id');
        $users = User::findOrfail($id);
        return view('admin.module.user.edit', ['user' => $users])->with('profession', $profession);
    }


    public function update(Request $request, $id)
    {
        $user = User::findOrfail($id);
        $userData = $request->only('name', 'email', 'profession_id');
        $user->update($userData);
        $profileData = $request->only('first_name', 'last_name', 'gender', 'father_name', 'mother_name', 'mobile', 'address', 'website', 'facebook', 'git', 'linkedin');
        $user->profile()->update($profileData);
        Session::flash('message', 'Data Update Successfully');
        return redirect('/user');
    }

    public function destroy($id)
    {
        User::destroy($id);
        Session::flash('message', 'Data Delete Successfully');
        return redirect('/user');
    }
}

