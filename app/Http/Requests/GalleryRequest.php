<?php

namespace App\Http\Requests;

use App\Gallery;
use App\User;
use Illuminate\Foundation\Http\FormRequest;

class GalleryRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'name'=>'required| min:4|unique:galleries,name,'.$this->get('id'),
            'description'=>'required | max:1000|min:10'
        ];
    }
}
