<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profession extends Model
{
    protected $fillable=['name'];
    public function user()
    {
        return $this->hasMany(User::class);
    }
    public function userVideos()
    {
        return $this->hasManyThrough(
            'App\Video', 'App\User',
            'profession_id', 'user_id', 'id'
        );
    }
    public  function articles()
    {
        return $this->hasManyThrough(Article::class,User::class);
    }
}
