@extends('layouts.blog.blogMaster')
@section('title','Hello article')
@section('content')
    <!-- Blog Post -->

    <!-- Title -->
    <h1>{{$article->title}}</h1>

    <!-- Author -->
    <p class="lead">
        by <a href="#">{{$article->user->name}}</a>
    </p>

    <hr>

    <!-- Date/Time -->
    <p><span class="glyphicon glyphicon-time"></span> Posted on {{$article->created_at}}</p>

    <hr>

    <!-- Preview Image -->
    <img class="img-responsive" src="http://placehold.it/900x300" alt="">

    <hr>

    <!-- Post Content -->
    <p class="lead">{!! $article->summery !!}</p>
    <p>{!! $article->details !!}</p>

    <hr>

    <!-- Blog Comments -->

    <!-- Comments Form -->
    <div class="well">
        <h4>Leave a Comment:</h4>
        <form role="form">
            <div class="form-group">
                <textarea class="form-control" rows="3"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

    <hr>

@foreach($comments as $key=>$comment)

    <div class="media">
        <a class="pull-left" href="#">
            <img class="media-object" src="http://placehold.it/64x64" alt="">
        </a>
        <div class="media-body">
            <h4 class="media-heading">{{ $comment->user->name }}
                <small>{{ $comment->created_at }}</small>
            </h4>
        {{ $comment->comment }}
                    @if($comment->comment_id!=NULL AND $comment->comment_id==$key)

                        <!-- Nested Comment -->
                        <div class="media">
                            <a class="pull-left" href="#">
                                <img class="media-object" src="http://placehold.it/64x64" alt="">
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">{{ $comment->user->name }}
                                    <small>{{ $comment->created_at }}</small>
                                </h4>
                                {{ $comment->comment }}
                             </div>

                            </div>
                    @endif
            <!-- End Nested Comment -->
        </div>
    </div>

    @endforeach

@endsection
@php
function checkChild($id)
{

}
@endphp