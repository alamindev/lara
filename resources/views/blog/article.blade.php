@extends('layouts.blog.blogMaster')
@section('title','Hello article')
@section('content')
    <h1 class="page-header">
        Page Heading
        <small>Secondary Text</small>
    </h1>
    {{--{{dd($articles)}}--}}
@foreach($articles as $article)
    <!-- First Blog Post -->
    <h2>
        <a href="{{url('catid',[$article->id,'article',$article->slug])}}">{{$article->title}}</a>
    </h2>
    <p class="lead">
        by <a href="{{url('user',['article',$article->user->id])}}">{{$article->user->name}}</a>
    </p>
    <p><span class="glyphicon glyphicon-time"></span> Posted on {{$article->created_at}}</p>
    <hr>
    <img class="img-responsive" src="http://placehold.it/900x300" alt="">
    <hr>
    <p>{{$article->summery}}</p>
    <a class="btn btn-primary" href="#">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>

    <hr>
@endforeach
    <!-- Pager -->
    <ul class="pager">
{{ $articles->links() }}
    </ul>
@endsection
@section('profession')

        <div class="col-lg-6">
            <ul class="list-unstyled">
                @foreach($professions as $profession)
                    <li><a href="{{url('profession',['article',$profession->id])}}" >{{$profession->name}}</a></li>
                    @endforeach
            </ul>
        </div>
@endsection