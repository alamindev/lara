@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard  |  <a href="/gallery">Back To the gallery List</a></div>
                    <div class="panel-body">
                    <div class="col-md-9 col-md-offset-1">
                        <h2>{{ $gallery->name}}</h2>
                        <h2>{{ $gallery->description}}</h2>
                        <h2>Display : {{ $gallery->display=='Y'?'Enable':'Disable'}}</h2>
                        @foreach($videos as $video)
                            <div class="col-md-12">
                                <div class="col-md-12">{{$video->title or null}}</div>
                                <div class="col-md-8">
                                    @if($video->provider=='Y')
                                        @include('admin.module.elements.youtube',['source'=>$video->source,'title'=>$video->title])
                                    @else
                                        @include('admin.module.elements.facebook',['source'=>$video->source,'title'=>$video->title])
                                    @endif
                                </div>
                            </div>
                        @endforeach
                        <a class="btn btn-xs btn-primary" href="{{url('/gallery/'.$gallery->id.'/edit')}}">Edit</a>
                        {{ Form::open(['url' => ['gallery', $gallery->id], 'method' => 'delete' ]) }}
                        <button class="btn btn-xs btn-danger" type="submit">Delete</button>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection