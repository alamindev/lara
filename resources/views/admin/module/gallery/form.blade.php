<div class="form-group">
    {!! Form::label('Name of gallery',null ,['class'=>'control-label col-md-3']) !!}
    <div class="col-md-9">
        {!! Form::text('name',isset($gallery->name)?$gallery->name:null,['class'=>"form-control",'placeholder'=>'The Name of the gallery']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('Description of gallery',null ,['class'=>'control-label col-md-3']) !!}
    <div class="col-md-9">
        {!! Form::text('description',isset($gallery->description)?$gallery->description:null,['class'=>"form-control",'placeholder'=>'The summery of the gallery']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('Display',null ,['class'=>'control-label col-md-3']) !!}
    <div class="col-md-9">
        {!! Form::radio('display','Y',isset($gallery->display)?$gallery->display=='Y'?'true' :false:null) !!} Yes
        {!! Form::radio('display','N',isset($gallery->display)?$gallery->display=='N'?'true' :false:null) !!} No
    </div>
</div>
<div class="form-group">
    {!! Form::label('Gallery',null ,['class'=>'control-label col-md-3']) !!}
    <div class="col-md-9">
        @foreach($videos as $video)
            <div class="col-md-8">
                {!! Form::checkbox('video_id[]',$video->id,isset($selected)?in_array($video->id,$selected):null,['multiple' => true]) !!} {{ $video->title }}
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="{{$video->source}}" allowfullscreen></iframe>
                    </div>
            </div>
        @endforeach
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-2 col-md-9">
        {!! Form::submit($formButton,['class'=>'btn btn-primary btn-lg']) !!}
    </div>
</div>