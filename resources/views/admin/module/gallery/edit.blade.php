@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard  |  <a href="/gallery">Back To Gallery List</a></div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            @include('admin.module.error');
                        </div>
                    {!! Form::open(['url' => '/gallery/'.$gallery->id, 'method'=>'PUT','class'=>'form-horizontal']) !!}
                        @include('admin.module.gallery.form',['formButton'=>'Update Gallery'])
                        {!! Form::hidden('id', $gallery->id) !!}
                        {!! Form::close() !!}
                    </div>
                    </div>
                </div>
            </div>
        </div>
@endsection