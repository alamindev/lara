<div class="embed-responsive embed-responsive-16by9" style="position:relative">

    <iframe class="embed-responsive-item" src="{{$source}}{{strpos($source,'?')?'&controls=0&showinfo=0&rel=0':'?controls=0&showinfo=0&rel=0'}}" allowfullscreen></iframe>
    <h4 style="position:absolute;color:#fff;font-family:arial; top:0px; margin: 0;left:5px; background: #333; background:-moz-linear-gradient(#333, transparent);background:-webkit-linear-gradient(#333, transparent); background:-o-linear-gradient(#333, transparent); background:linear-gradient(#333, transparent);">{{ $title}}</h4>
</div>