@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard  |  <a href="/profession">Back To User List</a></div>
                    <div class="panel-body">

                        {!! Form::open(['url' => '/profession', 'method'=>'POST','class'=>'form-horizontal']) !!}

                        <div class="form-group">
                            {!! Form::label('Name of Profession',null,['class'=>'control-label col-md-3']) !!}
                            <div class="col-md-9">
                                {!! Form::select('name',null,['class'=>"form-control",'placeholder'=>'Ex. Engineer, Doctor']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-9">
                                {!! Form::submit('Submit',['class'=>'btn btn-primary btn-lg']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection