@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard  |  <a href="/profession/create">Add Profession</a></div>
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    <div class="panel-body">
                        <table class="table-responsive table">
                            <thead>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            @foreach($professions as $profession)
                                <tr>
                                    <td>{{$profession->id or NULL}}</td>
                                    <td>{{$profession->name or NULL}}</td>
                                    <td width="15%"><a class="btn btn-xs btn-success" href="{{url('/profession/'.$profession->id)}}">View</a>
                                        <a class="btn btn-xs btn-primary" href="{{url('/profession/'.$profession->id.'/edit')}}">Edit</a>
                                        {{ Form::open(['url' => ['profession', $profession->id], 'method' => 'delete' ]) }}
                                        <button class="btn btn-xs btn-danger" type="submit">Delete</button>
                                    {{ Form::close() }}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@endsection