@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard  |  <a href="/user/create">Add User</a></div>
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    <div class="panel-body">
                        <table class="table-responsive table">
                            <thead>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Profession</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                <td>{{$user->name or NULL}}</td>
                                <td>{{$user->email or NULL}}</td>
                                <td>{{$user->profile->mobile or NULL}}</td>
                                <td>{{$user->profession->name or NULL}}</td>
                                <td width="15%"><a class="btn btn-xs btn-success" href="{{url('/user/'.$user->id)}}">View</a>
                                    <a class="btn btn-xs btn-primary" href="{{url('/user/'.$user->id.'/edit')}}">Edit</a>
                                    {{ Form::open(['url' => ['user', $user->id], 'method' => 'delete' ]) }}
                                    <button class="btn btn-xs btn-danger" type="submit">Delete</button>
                                    {{ Form::close() }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection