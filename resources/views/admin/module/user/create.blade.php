@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard  |  <a href="/user">Back To User List</a></div>
                    <div class="panel-body">

                    {!! Form::open(['url' => '/user', 'method'=>'POST','class'=>'form-horizontal']) !!}

                        <div class="form-group">
                            {!! Form::label('Name of User',null,['class'=>'control-label col-md-2']) !!}
                            <div class="col-md-2">
                            {!! Form::text('first_name',null,['class'=>"form-control",'placeholder'=>'First Name']) !!}
                            </div>
                            <div class="col-md-3">
                                {!! Form::text('name',null,['class'=>"form-control",'placeholder'=>'Name']) !!}
                            </div>
                            <div class="col-md-4">
                                {!! Form::text('last_name',null,['class'=>"form-control",'placeholder'=>'Last Name']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('Profession',null,['class'=>'control-label col-md-3']) !!}
                            <div class="col-md-9">
                                {!! Form::select('profession_id',$profession,null,['class'=>"form-control", 'placeholder'=>'Select Profession']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('Email',null,['class'=>'control-label col-md-2']) !!}
                            <div class="col-md-3">
                                {!! Form::text('email',null,['class'=>"form-control",'placeholder'=>'example@domainname.com']) !!}
                            </div>
                            {!! Form::label('Gender',null,['class'=>'control-label col-md-1']) !!}
                            <div class="col-md-2">
                                {!! Form::select('gender',['male'=>'Male','female'=>'Female'],null,['class'=>"form-control"]) !!}
                            </div>
                            {!! Form::label('Mobile',null,['class'=>'control-label col-md-1']) !!}
                            <div class="col-md-2">
                                {!! Form::text('mobile',null,['class'=>"form-control",'placeholder'=>'01XXXXXXXX']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('Father',null,['class'=>'control-label col-md-2']) !!}
                            <div class="col-md-4">
                                {!! Form::text('father_name',null,['class'=>"form-control",'placeholder'=>'Father Name']) !!}
                            </div>
                            {!! Form::label('Mother',null,['class'=>'control-label col-md-1']) !!}
                            <div class="col-md-4">
                                {!! Form::text('mother_name',null,['class'=>"form-control",'placeholder'=>'Mother Name']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('Website',null,['class'=>'control-label col-md-2']) !!}
                            <div class="col-md-4">
                                {!! Form::url('website',null,['class'=>"form-control",'placeholder'=>'https://yourwebsite.com']) !!}
                            </div>
                            {!! Form::label('Facebook',null,['class'=>'control-label col-md-1']) !!}
                            <div class="col-md-4">
                                {!! Form::url('facebook',null,['class'=>"form-control",'placeholder'=>'Your Facebook URL']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('Git Address',null,['class'=>'control-label col-md-2']) !!}
                            <div class="col-md-4">
                                {!! Form::url('git',null,['class'=>"form-control",'placeholder'=>'Your GitLab/GitHub URL']) !!}
                            </div>
                            {!! Form::label('Linkedin',null,['class'=>'control-label col-md-1']) !!}
                            <div class="col-md-4">
                                {!! Form::url('linkedin',null,['class'=>"form-control",'placeholder'=>'Your Linkedin URL']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('User Address',null,['class'=>'control-label col-md-2']) !!}
                            <div class="col-md-9">
                                {!! Form::textarea('address',null,['class'=>"form-control",'placeholder'=>'You Full Address', 'rows'=>'4']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('Password',null,['class'=>'control-label col-md-2']) !!}
                            <div class="col-md-4">
                                {!! Form::password('password',['class'=>"form-control",'placeholder'=>'********']) !!}
                            </div>
                            {!! Form::label('Retype Password',null,['class'=>'control-label col-md-1']) !!}
                            <div class="col-md-4">
                                {!! Form::password('password',['class'=>"form-control",'placeholder'=>'Retype Same Password']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-9">
                                {!! Form::submit('Submit',['class'=>'btn btn-primary btn-lg']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>

                    </div>
                </div>
            </div>
        </div>
@endsection