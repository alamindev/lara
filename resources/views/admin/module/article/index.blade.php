@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard  |  <a href="/article/create">Add article</a></div>
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    <div class="panel-body">
                        <table class="table-responsive table">
                            <thead>
                            <th>Title</th>
                            <th>Display</th>
                            <th>Summery</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            @foreach($articles as $article)
                                <tr>
                                <td>{!! $article->title or NULL !!}</td>
                                <td>{!! $article->display or NULL !!}</td>
                                <td>{!! $article->summery or NULL !!}</td>
                                <td width="15%"><a class="btn btn-xs btn-success" href="{{url('/article/'.$article->id)}}">View</a>
                                    <a class="btn btn-xs btn-primary" href="{{url('/article/'.$article->id.'/edit')}}">Edit</a>
                                    {{ Form::open(['url' => ['article', $article->id], 'method' => 'delete' ]) }}
                                    <button class="btn btn-xs btn-danger" type="submit">Delete</button>
                                    {{ Form::close() }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $articles->links() }}
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection