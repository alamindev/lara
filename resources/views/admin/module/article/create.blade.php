@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard  |  <a href="/article">Back To article List</a></div>
                    <div class="panel-body">

                    {!! Form::open(['url' => '/article', 'method'=>'POST','class'=>'form-horizontal']) !!}
                    @include('admin.module.article.form')
                    {!! Form::close() !!}
                    </div>

                    </div>
                </div>
            </div>
        </div>
@endsection