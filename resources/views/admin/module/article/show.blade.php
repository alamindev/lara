@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard  |  <a href="/user">Back To the User List</a></div>
                    <div class="panel-body">
                    <div class="col-md-9 col-md-offset-1">
                        <h2>{{ $user->profile->first_name}} {{ $user->name}} {{ $user->profile->last_name}}</h2>
                        <a class="btn btn-xs btn-primary" href="{{url('/user/'.$user->id.'/edit')}}">Edit</a>

                        {{ Form::open(['url' => ['user', $user->id], 'method' => 'delete' ]) }}
                        <button class="btn btn-xs btn-danger" type="submit">Delete</button>
                        {{ Form::close() }}

                        <h3>{{ucfirst($user->profile->gender)}}</h3>
                        <h4>Profession:  {{$user->profession->name}}</h4>
                        <h4>Email:  {{$user->email}}</h4>
                        <h4>Contact:  {{$user->profile->mobile}}</h4>
                        <h5>Father: {{ $user->profile->father_name}} </h5>
                        <h5>Mother: {{ $user->profile->mother_name}}  </h5>
                        <p>
                            {{$user->profile->address}}
                        </p>
                        <h3>Online Url</h3>
                        <ul>
                        <li type="none"><a href="{{$user->profile->website}}">{{$user->profile->website}}</a></li>
                        <li type="none"><a href="{{$user->profile->facebook}}">{{$user->profile->facebook}}</a></li>
                        <li type="none"><a href="{{$user->profile->git}}">{{$user->profile->git}}</a></li>
                        <li type="none"><a href="{{$user->profile->linkedin}}">{{$user->profile->linkedin}}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection