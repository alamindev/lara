<div class="form-group">
    {!! Form::label('Title of article',null,['class'=>'control-label col-md-3']) !!}
    <div class="col-md-9">
        {!! Form::text('title',null,['class'=>"form-control",'placeholder'=>'Article Title']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('Sub Title of Article',null,['class'=>'control-label col-md-3']) !!}
    <div class="col-md-9">
        {!! Form::text('sub_title',null,['class'=>"form-control",'placeholder'=>'Subtitle of Title']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('Slug of the article',null,['class'=>'control-label col-md-3']) !!}
    <div class="col-md-9">
        {!! Form::text('slug',null,['class'=>"form-control",'placeholder'=>'Subtitle of Title']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('Display',null ,['class'=>'control-label col-md-3']) !!}
    <div class="col-md-9">
        {!! Form::radio('display','Y',isset($video->display)?$video->display=='Y'?'true' :false:null) !!} Yes
        {!! Form::radio('display','N',isset($video->display)?$video->display=='N'?'true' :false:null) !!} No
    </div>
</div>
<div class="form-group">
    {!! Form::label('Summery',null,['class'=>'control-label col-md-3']) !!}
    <div class="col-md-9">
        {!! Form::textarea('summery',null,['class'=>"form-control",'id'=>'summery','placeholder'=>'Summery of the article']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('Details of Article',null,['class'=>'control-label col-md-3']) !!}
    <div class="col-md-9">
        {!! Form::textarea('details',null,['class'=>"form-control",'id'=>'details','placeholder'=>'Details of Title']) !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-3 col-md-9">
        {!! Form::submit('Submit',['class'=>'btn btn-primary btn-lg']) !!}
    </div>
</div>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'summery' );
    CKEDITOR.replace( 'details' );
</script>
