@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard  |  <a href="/video">Back To Video List</a></div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            @include('admin.module.error');
                        </div>

                    {!! Form::open(['url' => '/video/'.$video->id, 'method'=>'PUT','class'=>'form-horizontal']) !!}
                        @include('admin.module.video.form',['formButton'=>'Update Video'])
                        {!! Form::hidden('id', $video->id) !!}
                        {!! Form::close() !!}
                    </div>

                    </div>
                </div>
            </div>
        </div>
@endsection