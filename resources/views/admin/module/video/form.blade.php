<div class="form-group">
    {!! Form::label('Title of video',null ,['class'=>'control-label col-md-3']) !!}
    <div class="col-md-9">
        {!! Form::text('title',@isset($video->title)?$video->title:null,['class'=>"form-control",'placeholder'=>'The title of the video']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('Summery of video',null ,['class'=>'control-label col-md-3']) !!}
    <div class="col-md-9">
        {!! Form::text('summery',isset($video->summery)?$video->summery:null,['class'=>"form-control",'placeholder'=>'The summery of the video']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('Provider',null ,['class'=>'control-label col-md-3']) !!}
    <div class="col-md-9">
        {!! Form::radio('provider','Y',isset($video->provider)?$video->provider=='Y'?'true' :false:null) !!} Youtube
        {!! Form::radio('provider','F',isset($video->provider)?$video->provider=='F'?'true' :false:null) !!} Facebook
    </div>
</div>
<div class="form-group">
    {!! Form::label('Source',null ,['class'=>'control-label col-md-3']) !!}
    <div class="col-md-9">
        {!! Form::text('source',isset($video->source)?$video->source:null,['class'=>"form-control",'placeholder'=>'Ex. Engineer, DoctorThe title of the video']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('Display',null ,['class'=>'control-label col-md-3']) !!}
    <div class="col-md-9">
        {!! Form::radio('display','Y',isset($video->display)?$video->display=='Y'?'true' :false:null) !!} Yes
        {!! Form::radio('display','N',isset($video->display)?$video->display=='N'?'true' :false:null) !!} No
    </div>
</div>
<div class="form-group">
    {!! Form::label('Gallery',null ,['class'=>'control-label col-md-3']) !!}
    <div class="col-md-9">
        {!! Form::select('gallery_id[]',$galleries,isset($selected)?$selected:null,['class'=>"form-control",'id'=>'galleryselect','multiple' => true]) !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-2 col-md-9">
        {!! Form::submit($formButton,['class'=>'btn btn-primary btn-lg']) !!}
    </div>
</div>
