@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard | <a href="/video">Back To the video List</a></div>
                    <div class="panel-body">
                        <div class="col-md-9 col-md-offset-1">
                            <h2>{{ $video->title}}</h2>
                            <div class="col-md-12">
                                <p>{{ $video->summery}}</p>
                                <h4>{{ $video->provider}}</h4>
                            </div>
                            <a class="btn btn-xs btn-primary" href="{{url('/video/'.$video->id.'/edit')}}">Edit</a>
                            {{ Form::open(['url' => ['video', $video->id], 'method' => 'delete' ]) }}
                            <button class="btn btn-xs btn-danger" type="submit">Delete</button>
                            {{ Form::close() }}
                            <div>
                                @if($source->profider=='Y')
                                    @include('admin.module.elements.youtube',['source'=>$video->source,'title'=>$video->title])
                                @else
                                    @include('admin.module.elements.facebook',['source'=>$video->source,'title'=>$video->title])
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>@endsection