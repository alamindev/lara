@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard  |  <a href="/video/create">Add video</a></div>
                    @if (Session::has('message'))
                        <div class="slideupalert alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    <div class="panel-body">
                        <table class="table-responsive table">
                            <thead>
                            <th>Title</th>
                            <th>Gallery</th>
                            <th>Video</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                                 @foreach($videos as $video)
                                <tr>
                                    <td>{{$video->title or null}}</td>
                                    <td>
                                        @foreach($video->galleries as $gallery)

                                            <a href="{{url('gallery',$gallery->id)}}" class="btn btn-primary">{{$gallery->name}}</a>
                                            @endforeach
                                    </td>
                                    <td class="col-md-4">
                                        @if($video->provider=='Y')
                                            @include('admin.module.elements.youtube',['source'=>$video->source,'title'=>$video->title])
                                        @else
                                            @include('admin.module.elements.facebook',['source'=>$video->source,'title'=>$video->title])
                                        @endif
                                    </td>
                                    <td width="15%"><a class="btn btn-xs btn-success" href="{{url('/video/'.$video->id)}}">View</a>
                                        <a class="btn btn-xs btn-primary" href="{{url('/video/'.$video->id.'/edit')}}">Edit</a>
                                        {{ Form::open(['url' => ['video', $video->id], 'method' => 'delete','onsubmit' => 'ConfirmDelete()']) }}
                                        <button class="btn btn-xs btn-danger" type="submit">Delete</button>
                                        {{ Form::close() }}</td>
                                    <script>

                                        function ConfirmDelete()
                                        {
                                            var x = confirm("Are you sure you want to delete?");
                                            if (x)
                                                return true;
                                            else
                                                return false;
                                        }

                                    </script>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection