<!DOCTYPE html>
<html lang="en">
<head>
@include('layouts.blog.partial.head')
</head>
<body>
    <!-- Navigation -->
    @include('layouts.blog.partial.nav')

    <!-- Page Content -->
    <div class="container" style="padding-top: 90px;">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">

               @yield('content')

            </div>

            <!-- Blog Sidebar Widgets Column -->
            @include('layouts.blog.partial.sidebar')

        </div>
        <!-- /.row -->

        <hr>
    @include('layouts.blog.partial.footer')
    </div>

</body>

</html>
