<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        {{ ucfirst($menu) }} <span class="caret"></span>
    </a>

    <ul class="dropdown-menu" role="menu">
        <li> <a href="{{ url('/'.$menu) }}" > {{ucfirst($menu)}} list</a> </li>
        <li> <a href="{{ url('/'.$menu.'/create') }}" > {{ucfirst($menu)}} Add</a> </li>
    </ul>
</li>