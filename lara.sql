-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 08, 2016 at 03:42 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lara`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(20, '2014_10_12_000000_create_users_table', 1),
(21, '2014_10_12_100000_create_password_resets_table', 1),
(22, '2016_11_04_055154_ProfileTableMigrate', 1),
(23, '2016_11_08_120240_Prefession_table_create_Migration', 1),
(24, '2016_11_08_120545_UserTableAddColumnPrefessionId', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `professions`
--

CREATE TABLE `professions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `professions`
--

INSERT INTO `professions` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Engineer', NULL, '2016-11-08 07:20:45', NULL),
(2, 'Developer', NULL, NULL, NULL),
(3, 'Politician', '2016-11-08 06:43:48', '2016-11-08 06:52:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` enum('male','female') COLLATE utf8_unicode_ci NOT NULL,
  `father_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mother_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `website` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` text COLLATE utf8_unicode_ci NOT NULL,
  `git` text COLLATE utf8_unicode_ci NOT NULL,
  `linkedin` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `first_name`, `last_name`, `gender`, `father_name`, `mother_name`, `mobile`, `address`, `website`, `facebook`, `git`, `linkedin`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Prof.', 'Paucek', 'male', 'Prof. Erich', 'Ms. Gia', '+1-490-580-1954', '40220 Elmira Shores Apt. 724\nWest Alize, CT 14354', 'http://www.hermann.com/minima-animi-delectus-ipsa-ex-id-voluptates', 'https://www.facebook.com/Claire', 'https://www.github.com/Gilda', 'https://www.linkedin.com/Pearline', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(2, 2, 'Prof.', 'Gaylord', 'female', 'Dr. Geo', 'Mrs. Ressie', '684-986-2687 x0399', '3457 Kihn Lake\nWest Anita, AL 89994-1012', 'http://www.luettgen.com/', 'https://www.facebook.com/Walton', 'https://www.github.com/Jaleel', 'https://www.linkedin.com/Albina', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(3, 3, 'Prof.', 'Schamberger', 'male', 'Mr. Jasper', 'Miss Elnora', '1-552-699-3520 x2253', '735 Marvin Burg Suite 264\nWest Jermain, CO 53800-1510', 'http://www.fisher.com/error-ad-pariatur-est-accusamus-dolore', 'https://www.facebook.com/Ubaldo', 'https://www.gitlab.com/Vincenzo', 'https://www.linkedin.com/Craig', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(4, 4, 'Ms.', 'Rau', 'male', 'Prof. Pete', 'Dr. Rebeka', '(658) 886-7638 x07276', '93020 Dannie Grove Apt. 816\nEast Vito, CA 75838', 'http://www.graham.com/temporibus-commodi-quia-rerum-molestias-sequi-quo.html', 'https://www.facebook.com/Rebeka', 'https://www.github.com/Elmira', 'https://www.linkedin.com/Oran', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(5, 5, 'Mr.', 'Christiansen', 'female', 'Dr. Mekhi', 'Ms. Zella', '+1-765-691-9209', '8348 Stroman Glens Suite 453\nSoniaville, AZ 63084-3395', 'http://wiza.com/', 'https://www.facebook.com/Barrett', 'https://www.gitlab.com/Loma', 'https://www.linkedin.com/Eden', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(6, 6, 'Miss', 'Breitenberg', 'female', 'Mr. Brock', 'Miss Caitlyn', '(316) 333-6716 x27272', '5418 Harvey Turnpike Apt. 911\nNorth Maci, DE 30166', 'http://medhurst.com/', 'https://www.facebook.com/Louvenia', 'https://www.gitlab.com/Luigi', 'https://www.linkedin.com/Roderick', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(7, 7, 'Dr.', 'Collier', 'female', 'Mr. Paolo', 'Dr. Bernadette', '(584) 810-1695 x173', '24563 Swift Islands\nEast Archibald, SC 01128', 'http://www.quitzon.com/', 'https://www.facebook.com/Otto', 'https://www.gitlab.com/Berneice', 'https://www.linkedin.com/Rick', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(8, 8, 'Dr.', 'Wilderman', 'male', 'Prof. Frederic', 'Ms. Kelsie', '949.860.5222 x4398', '213 Parker Field\nKubstad, MS 61484', 'http://mills.com/deserunt-molestiae-est-quidem-et-ipsam-omnis-in-fugit', 'https://www.facebook.com/Rex', 'https://www.gitlab.com/Kasandra', 'https://www.linkedin.com/Estel', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(9, 9, 'Dr.', 'Mueller', 'male', 'Prof. Anibal', 'Miss Tiffany', '598.615.0443 x210', '260 Ava Manors\nPort Nels, NE 21100-3889', 'https://lind.com/provident-vel-enim-nulla-quia-ut-corporis.html', 'https://www.facebook.com/Kirstin', 'https://www.github.com/Erika', 'https://www.linkedin.com/Aleen', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(10, 10, 'Dr.', 'Pollich', 'male', 'Dr. Nils', 'Ms. Andreane', '731-797-4587', '37599 Lilla Manors\nEmoryport, AZ 88188', 'http://www.koch.com/quasi-architecto-earum-aut-reprehenderit-aut-quae-cumque.html', 'https://www.facebook.com/Ashlynn', 'https://www.gitlab.com/Orrin', 'https://www.linkedin.com/Barry', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(11, 11, 'Prof.', 'Hoeger', 'female', 'Prof. Kody', 'Ms. Noemi', '(885) 787-7666', '7221 Schmitt Cove\nSouth Ted, SC 15355', 'https://bogisich.org/vel-maiores-id-vitae-facilis-temporibus-pariatur.html', 'https://www.facebook.com/Magali', 'https://www.github.com/Dora', 'https://www.linkedin.com/Stella', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(12, 12, 'Dr.', 'Auer', 'female', 'Prof. Rex', 'Dr. Halie', '1-885-336-2733', '9151 Colby Keys Suite 060\nNorth Dwightfurt, NV 95624-1457', 'http://www.goldner.org/consequatur-in-nam-autem-nobis-asperiores-sunt-nostrum.html', 'https://www.facebook.com/Ciara', 'https://www.gitlab.com/Cecil', 'https://www.linkedin.com/Jace', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(13, 13, 'Prof.', 'Torp', 'female', 'Prof. Kale', 'Ms. Elsa', '(621) 372-0865', '3420 Murray Junction\nDeckowfort, RI 54606-8902', 'https://russel.com/iste-et-sed-nesciunt-ut.html', 'https://www.facebook.com/Dereck', 'https://www.gitlab.com/Rudolph', 'https://www.linkedin.com/Florencio', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(14, 14, 'Mr.', 'Feeney', 'female', 'Prof. Kristopher', 'Ms. Alene', '+1-656-320-8770', '79147 Ledner Inlet\nWest Tressa, MT 17687-7478', 'http://www.jones.com/quia-minima-natus-consequatur-aspernatur-iure-corrupti', 'https://www.facebook.com/Darrell', 'https://www.github.com/Agnes', 'https://www.linkedin.com/Ellie', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(15, 15, 'Prof.', 'Green', 'female', 'Dr. Trevion', 'Dr. Rosemary', '545-984-3754 x8945', '765 Yundt Oval\nLelahmouth, MN 32753', 'https://mclaughlin.com/vel-corrupti-ut-rerum-iure-ab.html', 'https://www.facebook.com/Cordie', 'https://www.gitlab.com/Hosea', 'https://www.linkedin.com/Bernita', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(16, 16, 'Mrs.', 'Simonis', 'female', 'Mr. Kacey', 'Miss Rosanna', '+1 (591) 776-4151', '524 Percival Radial\nMaggiofurt, WY 59165', 'http://www.kertzmann.org/aut-et-odit-ipsum-culpa-quis-porro-quos', 'https://www.facebook.com/Rogers', 'https://www.gitlab.com/Timmy', 'https://www.linkedin.com/Imelda', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(17, 17, 'Dr.', 'Cartwright', 'female', 'Mr. Fermin', 'Dr. Agnes', '1-534-467-7064 x76634', '406 Walsh Fields\nReichelfort, CT 17411', 'http://www.yost.info/quis-sunt-et-ullam-sed-est-nam-asperiores', 'https://www.facebook.com/Kariane', 'https://www.github.com/Buck', 'https://www.linkedin.com/Pattie', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(18, 18, 'Ms.', 'Abbott', 'male', 'Prof. Francis', 'Prof. Shaniya', '573.672.8234 x53212', '108 Ramon Green Apt. 783\nNorth Rey, WY 17761-6109', 'https://king.com/sed-reprehenderit-illo-rerum-sint-aperiam-est.html', 'https://www.facebook.com/Allison', 'https://www.github.com/Geraldine', 'https://www.linkedin.com/Prince', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(19, 19, 'Mr.', 'Turner', 'female', 'Prof. Gaetano', 'Miss Gilda', '+1-961-235-2993', '252 Gayle Crossroad\nWest Frederick, WV 29458', 'http://www.schiller.org/blanditiis-asperiores-sapiente-quasi-magnam-magni-reprehenderit', 'https://www.facebook.com/Nathaniel', 'https://www.gitlab.com/Maye', 'https://www.linkedin.com/Dean', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(20, 20, 'Prof.', 'Runolfsson', 'male', 'Mr. Markus', 'Mrs. Melba', '878-892-7580 x043', '444 Marvin Branch Suite 839\nPort Jaime, OK 87540-9679', 'http://www.brown.com/', 'https://www.facebook.com/Rosa', 'https://www.github.com/Dina', 'https://www.linkedin.com/Nasir', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(21, 21, 'Dr.', 'Bauch', 'male', 'Dr. Theodore', 'Ms. Martina', '1-810-238-6737 x19399', '15899 Halvorson Trail\nSwaniawskibury, ID 23877', 'http://shanahan.net/ipsum-et-repellendus-voluptatem-est-inventore.html', 'https://www.facebook.com/Autumn', 'https://www.gitlab.com/Ramiro', 'https://www.linkedin.com/Broderick', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(22, 22, 'Dr.', 'Pouros', 'female', 'Dr. Monserrate', 'Mrs. Angelita', '791.287.5257', '649 Abbigail Path\nAlvahbury, OK 05226', 'http://emard.info/non-id-quia-ad-totam-minima-atque-eius.html', 'https://www.facebook.com/Karl', 'https://www.github.com/Meda', 'https://www.linkedin.com/Reggie', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(23, 23, 'Ms.', 'Huels', 'male', 'Prof. Deshaun', 'Mrs. Ila', '1-997-530-9665 x264', '94478 Tomas Wall Suite 623\nPort Jayde, UT 05972', 'http://lockman.biz/non-aut-iure-perspiciatis-consequuntur-veritatis-veritatis', 'https://www.facebook.com/Kassandra', 'https://www.github.com/Abby', 'https://www.linkedin.com/Heber', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(24, 24, 'Mr.', 'Labadie', 'male', 'Dr. Morgan', 'Ms. Lilla', '(871) 680-7444 x470', '8472 Maxine Prairie\nWest Jacques, CT 11319-6104', 'http://stiedemann.com/', 'https://www.facebook.com/Kiara', 'https://www.gitlab.com/Clemmie', 'https://www.linkedin.com/Daija', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(25, 25, 'Miss', 'Hackett', 'female', 'Dr. Maynard', 'Ms. Letha', '(521) 543-1267', '7807 Pacocha Trail Suite 363\nWadefurt, SD 84808', 'http://lueilwitz.com/', 'https://www.facebook.com/Lon', 'https://www.gitlab.com/Morgan', 'https://www.linkedin.com/Gregg', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(26, 26, 'Prof.', 'Barton', 'female', 'Prof. Arch', 'Miss Paula', '753.559.9332', '77981 Cole Pike Suite 062\nWest Victor, AR 11120', 'https://www.bahringer.com/sapiente-cupiditate-sit-deleniti', 'https://www.facebook.com/Fredy', 'https://www.gitlab.com/Bo', 'https://www.linkedin.com/Rebekah', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(27, 27, 'Prof.', 'Huel', 'female', 'Mr. Jasen', 'Ms. Yvette', '(382) 467-6368 x41679', '7439 Floy Ferry Suite 680\nWest Kiley, ID 82179', 'http://www.miller.com/sit-corrupti-ratione-architecto-dolorem-ea-architecto-nesciunt.html', 'https://www.facebook.com/Amani', 'https://www.github.com/Serena', 'https://www.linkedin.com/Sunny', '2016-11-08 06:19:11', '2016-11-08 06:19:11', NULL),
(28, 28, 'Miss', 'Lebsack', 'female', 'Prof. Manuela', 'Prof. Mikayla', '402-977-1025 x3722', '408 Leilani Bridge Apt. 376\nPort Artton, MT 68247-0177', 'https://www.schultz.com/hic-iusto-et-natus-sit-aperiam-omnis-ratione', 'https://www.facebook.com/Rebeca', 'https://www.github.com/Dillan', 'https://www.linkedin.com/Frida', '2016-11-08 06:19:12', '2016-11-08 06:19:12', NULL),
(29, 29, 'Prof.', 'Klocko', 'male', 'Dr. Jayson', 'Mrs. Domenica', '+1.829.568.1587', '749 Will Trace\nSouth Lacy, OH 90599', 'http://kilback.info/recusandae-ipsum-repellendus-aut-velit-quas-nemo-quibusdam', 'https://www.facebook.com/Federico', 'https://www.github.com/Emmanuel', 'https://www.linkedin.com/Bartholome', '2016-11-08 06:19:12', '2016-11-08 06:19:12', NULL),
(30, 30, 'Mr.', 'DuBuque', 'female', 'Dr. Curt', 'Mrs. Stefanie', '1-997-433-5106 x897', '3437 Liliana Village Suite 593\nHowellmouth, NE 07588-7905', 'http://wilderman.com/possimus-est-laboriosam-expedita-consectetur-veniam-aliquam-libero.html', 'https://www.facebook.com/Brenden', 'https://www.gitlab.com/Alfredo', 'https://www.linkedin.com/Sammy', '2016-11-08 06:19:12', '2016-11-08 06:19:12', NULL),
(31, 31, 'Mr.', 'Schulist', 'male', 'Prof. Rowland', 'Mrs. Elisha', '(745) 891-8700 x55044', '152 Odie Ranch\nNorth Darrelmouth, CT 41220', 'http://www.hegmann.biz/eaque-quae-officiis-deleniti-vero-sed-repellat-odit', 'https://www.facebook.com/Frances', 'https://www.github.com/Julio', 'https://www.linkedin.com/Berry', '2016-11-08 06:19:12', '2016-11-08 06:19:12', NULL),
(32, 32, 'Dr.', 'White', 'male', 'Dr. Jillian', 'Dr. Neoma', '565-488-0777', '2470 Hollie Ports Suite 216\nMosciskifort, CO 19941', 'http://lowe.com/', 'https://www.facebook.com/Shakira', 'https://www.gitlab.com/Mireya', 'https://www.linkedin.com/Justen', '2016-11-08 06:19:12', '2016-11-08 06:19:12', NULL),
(33, 33, 'Prof.', 'Larson', 'male', 'Mr. Justyn', 'Dr. Alejandra', '(426) 913-3061', '4984 Seth Turnpike Suite 442\nPeteland, AK 43560-4697', 'https://rowe.info/et-in-ea-eum-aut-nihil-sit.html', 'https://www.facebook.com/Rubye', 'https://www.github.com/Pink', 'https://www.linkedin.com/Eleanore', '2016-11-08 06:19:12', '2016-11-08 06:19:12', NULL),
(34, 34, 'Miss', 'Hirthe', 'male', 'Mr. Chelsey', 'Ms. Luisa', '889-798-6032', '66484 Emery Meadows Suite 350\nNew Eladioport, IL 33676', 'http://blick.biz/et-omnis-sapiente-et-commodi-quod', 'https://www.facebook.com/Emmet', 'https://www.github.com/Phoebe', 'https://www.linkedin.com/Carmel', '2016-11-08 06:19:12', '2016-11-08 06:19:12', NULL),
(35, 35, 'Mr.', 'Lockman', 'female', 'Mr. Kadin', 'Prof. Lolita', '(946) 643-3026 x5280', '728 Vandervort Forges\nNorth Beaulahville, OR 59240', 'https://wilderman.net/rerum-excepturi-quisquam-eveniet-id-blanditiis-cum-perferendis.html', 'https://www.facebook.com/Candelario', 'https://www.gitlab.com/Ivy', 'https://www.linkedin.com/Tatum', '2016-11-08 06:19:12', '2016-11-08 06:19:12', NULL),
(36, 36, 'Miss', 'Koepp', 'female', 'Mr. Gavin', 'Ms. Sydnie', '1-459-838-4579 x089', '258 Beatty Points Apt. 524\nPercivalhaven, HI 79601', 'https://www.leuschke.info/ut-et-suscipit-debitis-itaque-sed', 'https://www.facebook.com/Oswald', 'https://www.github.com/Finn', 'https://www.linkedin.com/Claudie', '2016-11-08 06:19:12', '2016-11-08 06:19:12', NULL),
(37, 37, 'Dr.', 'Bartell', 'male', 'Mr. Darian', 'Prof. Aylin', '1-737-719-3370 x1062', '39418 Kyla Lodge Suite 393\nCassandrechester, NH 56753-6821', 'http://renner.com/reprehenderit-nostrum-fugit-ullam-dolores', 'https://www.facebook.com/Jordan', 'https://www.gitlab.com/Christophe', 'https://www.linkedin.com/Janick', '2016-11-08 06:19:12', '2016-11-08 06:19:12', NULL),
(38, 38, 'Ms.', 'Medhurst', 'male', 'Dr. Austen', 'Prof. Lenna', '+19205352276', '9124 Casey Pine\nLake Garfieldberg, MD 38224-3836', 'http://schoen.info/asperiores-fuga-eos-voluptas-est-eaque-aperiam-autem-reiciendis', 'https://www.facebook.com/Freddie', 'https://www.gitlab.com/Margaretta', 'https://www.linkedin.com/Willy', '2016-11-08 06:19:12', '2016-11-08 06:19:12', NULL),
(39, 39, 'Mrs.', 'Collins', 'female', 'Prof. Torrey', 'Dr. Luella', '942-334-2792 x3956', '29663 Weber Row Apt. 728\nBoydborough, DE 55447-9171', 'http://rice.com/corporis-earum-laboriosam-possimus-recusandae', 'https://www.facebook.com/Jeramy', 'https://www.github.com/Ressie', 'https://www.linkedin.com/Cielo', '2016-11-08 06:19:12', '2016-11-08 06:19:12', NULL),
(40, 40, 'Prof.', 'Beier', 'male', 'Prof. Anibal', 'Miss Maybell', '1-474-928-6066 x855', '279 Monty Mews\nGunnerbury, RI 27916-6090', 'http://www.dickinson.biz/excepturi-autem-nihil-nulla-dolor-aut-officiis-quidem', 'https://www.facebook.com/Leonel', 'https://www.gitlab.com/Abdul', 'https://www.linkedin.com/Alek', '2016-11-08 06:19:12', '2016-11-08 06:19:12', NULL),
(41, 41, 'Dr.', 'Reilly', 'female', 'Prof. Kelton', 'Mrs. Brenna', '(574) 389-6988', '231 D''Amore Well Apt. 588\nSouth Jovan, IL 04617-2806', 'https://mills.com/quam-soluta-consequatur-in-est-officia-eius-ad.html', 'https://www.facebook.com/Demarcus', 'https://www.gitlab.com/Deron', 'https://www.linkedin.com/Hildegard', '2016-11-08 06:19:12', '2016-11-08 06:19:12', NULL),
(42, 42, 'Dr.', 'O''Keefe', 'female', 'Mr. Al', 'Prof. Valerie', '+1-682-289-2605', '7501 Mathias Parkways Suite 322\nPort Orinfort, MO 27787', 'https://bradtke.com/vel-expedita-corporis-corrupti.html', 'https://www.facebook.com/Luz', 'https://www.gitlab.com/Edgardo', 'https://www.linkedin.com/Soledad', '2016-11-08 06:19:12', '2016-11-08 06:19:12', NULL),
(43, 43, 'Dr.', 'Deckow', 'female', 'Prof. Akeem', 'Prof. Lolita', '1-243-661-1227', '90156 Crona Way\nMarquardtchester, SC 19093', 'http://www.kerluke.com/', 'https://www.facebook.com/Dejah', 'https://www.github.com/Mazie', 'https://www.linkedin.com/Kara', '2016-11-08 06:19:12', '2016-11-08 06:19:12', NULL),
(44, 44, 'Prof.', 'Heidenreich', 'female', 'Dr. Reynold', 'Mrs. Margarete', '505-867-3163', '36545 Runolfsson Parkway Suite 214\nPort Malinda, LA 69001-6787', 'http://www.effertz.com/', 'https://www.facebook.com/Emiliano', 'https://www.github.com/Tyler', 'https://www.linkedin.com/Adelle', '2016-11-08 06:19:12', '2016-11-08 06:19:12', NULL),
(45, 45, 'Miss', 'Reichel', 'female', 'Prof. Daren', 'Miss Lilian', '342-841-7512', '582 Maxine Rest Apt. 139\nReynaville, LA 31549-6210', 'https://zemlak.net/dolorem-aut-nihil-totam-ut-eum-adipisci.html', 'https://www.facebook.com/Jaime', 'https://www.gitlab.com/Hortense', 'https://www.linkedin.com/Brendan', '2016-11-08 06:19:12', '2016-11-08 06:19:12', NULL),
(46, 46, 'Mr.', 'Morissette', 'female', 'Dr. Jan', 'Miss Ophelia', '(908) 999-1159 x172', '4573 O''Connell Summit\nCarrollshire, AK 30604-4985', 'http://www.bernier.org/ipsum-repudiandae-optio-laudantium-omnis-facere.html', 'https://www.facebook.com/Einar', 'https://www.gitlab.com/Jarvis', 'https://www.linkedin.com/Jaida', '2016-11-08 06:19:12', '2016-11-08 06:19:12', NULL),
(47, 47, 'Prof.', 'Jacobs', 'female', 'Dr. Ned', 'Miss Freeda', '248.337.7948 x193', '78772 Zack Roads Suite 219\nSavionborough, IL 39549-0869', 'http://www.kemmer.com/magnam-libero-unde-voluptas-incidunt-qui', 'https://www.facebook.com/Alexandra', 'https://www.github.com/Dena', 'https://www.linkedin.com/Marlin', '2016-11-08 06:19:12', '2016-11-08 06:19:12', NULL),
(48, 48, 'Mrs.', 'Sporer', 'female', 'Mr. Lowell', 'Mrs. Lisette', '(603) 930-5662', '602 Mitchell Manors Apt. 870\nKulasville, GA 91056-8396', 'http://www.block.com/', 'https://www.facebook.com/Amalia', 'https://www.gitlab.com/Vada', 'https://www.linkedin.com/Ilene', '2016-11-08 06:19:12', '2016-11-08 06:19:12', NULL),
(49, 49, 'Prof.', 'Robel', 'male', 'Mr. Elton', 'Ms. Martina', '(717) 829-8707 x136', '800 Cielo Isle Apt. 072\nSouth Kennedi, ID 65892-4846', 'http://hackett.org/', 'https://www.facebook.com/Adriana', 'https://www.gitlab.com/Zachary', 'https://www.linkedin.com/Ahmed', '2016-11-08 06:19:12', '2016-11-08 06:19:12', NULL),
(50, 50, 'Prof.', 'Kovacek', 'male', 'Prof. Hugh', 'Dr. Palma', '858-430-3683', '6536 Cora Village\nGordonfort, SD 96561', 'http://padberg.biz/ratione-vero-ex-eaque-ut-praesentium-exercitationem-porro-pariatur', 'https://www.facebook.com/Janie', 'https://www.gitlab.com/Paula', 'https://www.linkedin.com/Fatima', '2016-11-08 06:19:12', '2016-11-08 06:19:12', NULL),
(51, 51, 'Md', 'Hossain', 'male', 'Lat Abdul Latif', 'Mother', '019221324417', 'The address of rumen', 'https://website.com', 'https://facebook.com/rumen', 'https://gitlab.com/rumen', 'https://linedin.com/rumen', '2016-11-08 06:28:10', '2016-11-08 06:28:10', NULL),
(52, 52, 'Md', 'Hossain', 'male', 'Lat Abdul Latif', 'Mother', '0192213244173223', '', 'https://website.com', 'https://facebook.com/rumen', 'https://gitlab.com/rumen', 'https://linedin.com/rumen', '2016-11-08 07:18:56', '2016-11-08 07:20:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `profession_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `profession_id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'Dr. Cleora Bogisich DDS', 'alysson.little@example.net', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', '9LSatciQXw', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(2, 2, 'Rick Wiegand', 'balistreri.amya@example.com', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'R7Qe02cTBX', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(3, NULL, 'Mrs. Daniela Flatley', 'stan.spinka@example.com', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', '8FkZkoy9eV', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(4, NULL, 'Lilla Hilpert III', 'blarkin@example.org', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'iyXzHbASQr', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(5, NULL, 'Bernice Cassin', 'eschmeler@example.org', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'QusgldL96j', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(6, NULL, 'Destin Hodkiewicz', 'daniel.freeda@example.org', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'LJdzjjHT2S', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(7, NULL, 'Kevin Schuster', 'gtorphy@example.com', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'sIOQbTlMjk', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(8, NULL, 'Prof. Sofia Kirlin DVM', 'arlie.medhurst@example.com', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'Ugb2zDh06x', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(9, NULL, 'Dr. Clotilde Hoeger', 'fkunde@example.net', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'KupR9XFx70', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(10, NULL, 'Troy Rolfson', 'alejandra.rolfson@example.org', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'JFAdBTCU4e', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(11, NULL, 'Evelyn Howe Sr.', 'oliver41@example.org', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'RUhxnPpJ5W', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(12, NULL, 'Dr. Ibrahim Gleason III', 'ramiro33@example.net', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'CaoHSYiIxl', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(13, NULL, 'Aurelio Brekke', 'jason47@example.net', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'vBOQOXJo3E', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(14, NULL, 'Alan Auer', 'durward.kreiger@example.com', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'WhhVEQ6Mex', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(15, NULL, 'Abbie Ondricka', 'benjamin.friesen@example.org', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'bNL3cOd4tX', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(16, NULL, 'Nathen Nicolas DVM', 'qgreenfelder@example.com', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'IplS7MFnmn', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(17, NULL, 'Dillon Gerhold', 'mollie.dickens@example.net', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'iRuhWrN0G7', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(18, NULL, 'Dr. Devin Russel', 'shawna70@example.net', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'DT9QyWtrXg', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(19, NULL, 'Bethany Kuhlman', 'stefan09@example.net', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'OXBzaOJdqZ', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(20, NULL, 'Rex Cassin Sr.', 'demario27@example.net', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'fwBAP5Dhwq', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(21, NULL, 'Prof. Stuart Rippin', 'marty.cruickshank@example.net', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'DdHoXA50dA', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(22, NULL, 'Ottilie Rempel', 'gleichner.dallas@example.net', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'UDYTKkbDYI', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(23, NULL, 'Bryce Bahringer', 'david09@example.net', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'O49FVNtDx9', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(24, NULL, 'Leanne Koch', 'rollin.larson@example.org', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'ZmEDuBAIdf', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(25, NULL, 'Tanya Kessler', 'lora57@example.net', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'NyC36wBh5W', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(26, NULL, 'Collin Dibbert', 'aharris@example.org', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'NM2PVBAOKN', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(27, NULL, 'Dr. Hoyt Hermiston DDS', 'wfranecki@example.org', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', '6odGq1nuBo', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(28, NULL, 'Nelle Maggio', 'kirlin.judd@example.org', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'qH2Orn3Y6y', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(29, NULL, 'Brenda Armstrong DDS', 'mathew26@example.net', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', '0FoOdHAiX8', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(30, NULL, 'Andreane Kris I', 'jeffrey77@example.com', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', '0ZawyFkyBM', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(31, NULL, 'Alvina Quigley', 'rose26@example.com', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'RYxuT48luW', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(32, NULL, 'Josie Spinka', 'ziemann.emelia@example.com', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'i8Z2UZZmuc', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(33, NULL, 'Adrain Rau', 'sawayn.tyrell@example.org', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'j5nWCOkzVt', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(34, NULL, 'Jakayla Williamson', 'tkub@example.com', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'N5x8QBzgpM', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(35, NULL, 'Dr. Ericka Deckow DVM', 'eemmerich@example.org', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'HxSQ0wbtU1', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(36, NULL, 'Dalton Fadel', 'aurelia99@example.com', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'yJlbZskEhO', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(37, NULL, 'Lolita Kris', 'kaci.hickle@example.com', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'Bg2ClJbSdU', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(38, NULL, 'Juana Nikolaus', 'nikolaus.kathryn@example.org', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'cUj7zFZkw0', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(39, NULL, 'Laury Hirthe Jr.', 'qokeefe@example.net', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'zG7MF2gOnx', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(40, NULL, 'Dr. Gabe Carroll PhD', 'ukunze@example.net', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'dmkwYvLPgb', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(41, NULL, 'Mr. Hilario Botsford II', 'tyrell51@example.net', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'LU3HdY40VK', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(42, NULL, 'Thalia Schulist', 'eabshire@example.org', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'GEq8ehiXJ0', '2016-11-08 06:19:10', '2016-11-08 06:19:10'),
(43, NULL, 'Gudrun Reilly V', 'considine.jayson@example.net', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'o5o6ssq4i9', '2016-11-08 06:19:11', '2016-11-08 06:19:11'),
(44, NULL, 'Darian Larson V', 'bogisich.jannie@example.org', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', '7xlmUFOfvf', '2016-11-08 06:19:11', '2016-11-08 06:19:11'),
(45, NULL, 'Charlie Monahan', 'nlockman@example.org', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'r3Nwas3GEM', '2016-11-08 06:19:11', '2016-11-08 06:19:11'),
(46, NULL, 'Mrs. Electa Olson', 'cbosco@example.net', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'CevSP09y9X', '2016-11-08 06:19:11', '2016-11-08 06:19:11'),
(47, NULL, 'Izabella Bradtke', 'pjones@example.net', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'DZWMM4JTix', '2016-11-08 06:19:11', '2016-11-08 06:19:11'),
(48, NULL, 'Elliott Eichmann', 'frami.gilberto@example.org', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'nszd7BpZZo', '2016-11-08 06:19:11', '2016-11-08 06:19:11'),
(49, NULL, 'Prof. Roberta Ondricka PhD', 'bhaley@example.net', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', '9ycFk0PHyR', '2016-11-08 06:19:11', '2016-11-08 06:19:11'),
(50, NULL, 'Camila Hagenes', 'kutch.lavina@example.net', '$2y$10$cqU2dwtHZb9RunyCgqDVDuvQNb7lqZ6mmjuAgc11pUDiCh5ODDenq', 'g4r6gDAlEL', '2016-11-08 06:19:11', '2016-11-08 06:19:11'),
(51, 1, 'rumen', 'rumen@gmail.com', '$2y$10$laKJhiNeRyXf1x.JmKCNDOvRvg0seefuUwQmpu.M3hoYB7nDZzc4m', NULL, '2016-11-08 06:28:10', '2016-11-08 06:28:10'),
(52, 1, 'rumen', 'alamincse@hh.com', '$2y$10$p55ogRKWUxkgq4vCC3OqReYV0qKrP.rqq9nn3pkGzDCYwkX7dTsb6', NULL, '2016-11-08 07:18:56', '2016-11-08 07:20:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `professions`
--
ALTER TABLE `professions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `profiles_mobile_unique` (`mobile`),
  ADD KEY `profiles_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_profession_id_foreign` (`profession_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `professions`
--
ALTER TABLE `professions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_profession_id_foreign` FOREIGN KEY (`profession_id`) REFERENCES `professions` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
